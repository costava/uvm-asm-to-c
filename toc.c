//
// toc
//
// Synopsis: Transpile UVM assembly to C11
// Repo: https://codeberg.org/costava/uvm-asm-to-c
//

#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// malloc but if error, prints to stderr and exits.
void *mem_Alloc(const size_t size) {
	void *const ptr = malloc(size);

	if (ptr == NULL && size != 0) {
		fprintf(stderr, "%s: Failed to malloc %zu\n", __func__, size);
		exit(1);
	}

	return ptr;
}

// realloc but if error, prints to stderr and exits.
void *mem_Realloc(void *const ptr, const size_t newSize) {
	void *const newPtr = realloc(ptr, newSize);

	if (newPtr == NULL && newSize != 0) {
		fprintf(stderr, "%s: Failed to realloc pointer %p to %zu bytes\n",
			__func__, ptr, newSize);
		exit(1);
	}

	return newPtr;
}

bool SliceMatchesString(
	const uint8_t *const buf,
	const uint64_t sliceFirstIdx,
	const uint64_t sliceLen,
	const unsigned char *const str)
{
	for (uint64_t i = 0; i < sliceLen; i += 1) {
		if (str[i] == '\0' || str[i] != buf[sliceFirstIdx + i]) {
			return false;
		}
	}

	if (str[sliceLen] != '\0') {
		return false;
	}

	return true;
}

// Reads entire file at given path into given buf and populates given length var.
// On success, *out_fileBuf is pointer to null-terminated string.
// out_fileLen does not include the null terminator.
// On success: *out_fileBuf is from malloc
// On error:
// - Prints to stderr
// - calls exit(1)
/*@
  requires valid_read_string(path);
  requires \valid(out_fileLen);
  requires \separated(path, out_fileLen);

  assigns out_fileBuf[0];
  assigns out_fileLen[0];

  ensures 0 <= *out_fileLen <= SIZE_MAX;
  ensures 0 <= *out_fileLen <= UINT64_MAX;
*/
void io_ReadEntireFile(
	const char *const path,
	uint8_t *out_fileBuf[const static 1],
	uint64_t out_fileLen[const static 1])
{
	FILE *const f = fopen(path, "rb");

	if (f == NULL) {
		fprintf(stderr, "%s: Error calling fopen on %s\n", __func__, path);
		exit(1);
	}

	if (fseek(f, 0, SEEK_END) != 0) {
		fprintf(stderr, "%s: Failed to fseek to end of FILE* of %s\n", __func__, path);
		fclose(f);
		exit(1);
	}

	const long fileLen = ftell(f);

	if (fileLen == -1L) {
		fprintf(stderr, "%s: ftell error on FILE* of %s\n", __func__, path);
		perror("ftell");
		fclose(f);
		exit(1);
	}

	assert(fileLen >= 0);
	_Static_assert(LONG_MAX <= SIZE_MAX, "Runtime check needed");

	const size_t fileLenS = fileLen;

	if (fileLenS == SIZE_MAX) {
		fprintf(stderr, "%s: File %s is too long (%zu bytes)\n", __func__, path, SIZE_MAX);
		fclose(f);
		exit(1);
	}

	const size_t bufSize = fileLenS + 1;
	char *const buf = malloc(bufSize);

	if (buf == NULL) {
		fprintf(stderr,
			"%s: Error calling malloc with %zu for holding the contents of %s\n",
			__func__, bufSize, path);

		fclose(f);
		exit(1);
	}

	//@ assert \valid(&buf[0 .. bufSize-1]);

	if (fileLen > 0) {
		rewind(f);

		if (fread(buf, (size_t)fileLen, 1, f) != 1) {
			fprintf(stderr, "%s: fread error on FILE* of %s\n", __func__, path);

			fclose(f);
			free(buf);
			exit(1);
		}
	}

	if (fclose(f) != 0) {
		fprintf(stderr, "%s: fclose error on FILE* of %s\n", __func__, path);

		free(buf);
		exit(1);
	}

	buf[fileLen] = '\0';

	_Static_assert(LONG_MAX <= UINT64_MAX, "Runtime check needed");
	_Static_assert(CHAR_BIT == 8, "char type required to have 8 bits");

	*out_fileBuf = (uint8_t*)buf;
	*out_fileLen = (uint64_t)fileLen;
}

// Call fprintf. If error, exit(1)
#define IO_FPRINTF(...)             \
do {                                \
    if (fprintf(__VA_ARGS__) < 0) { \
        exit(1);                    \
    }                               \
} while (0)

// Call fputc. If error, exit(1)
#define IO_FPUTC(ch, stream)        \
do {                                \
    if (fputc(ch, stream) == EOF) { \
        exit(1);                    \
    }                               \
} while (0)

// Call fputs. If error, exit(1)
#define IO_FPUTS(str, stream)        \
do {                                 \
    if (fputs(str, stream) == EOF) { \
        exit(1);                     \
    }                                \
} while (0)

enum Section {
	SECTION_CODE = 1,
	SECTION_DATA = 2
};

// Return true if given character is whitespace, else returns false.
/*@
	assigns \nothing;
*/
bool IsWhitespace(const uint8_t c) {
	return (c == ' ') || (c == '\r') || (c == '\n') ||
	       (c == '\t');
}

// Return first index after the comment (right after the newline).
// Returns bufLen if comment goes to end of buf.
// Output the comment if currently in code section.
/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	requires 0 <= poundIdx < bufLen;
	requires buf[poundIdx] == '#';
	assigns \nothing;
	ensures poundIdx < \result <= bufLen;
*/
uint64_t HandleComment(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t poundIdx,
	FILE *const outFile,
	enum Section currentSection)
{
	uint64_t returnIdx = poundIdx + 1;

	/*@
		loop invariant (poundIdx + 1) <= returnIdx <= bufLen;
		loop assigns returnIdx;
		loop variant bufLen - returnIdx;
	*/
	while (returnIdx < bufLen) {
		if (buf[returnIdx] == '\n') {
			returnIdx += 1;
			break;
		}

		returnIdx += 1;
	}

	assert(poundIdx < returnIdx);

	const bool shouldOutputComment = (currentSection == SECTION_CODE);
	if (!shouldOutputComment) {
		return returnIdx;
	}

	// Step back to final character of buf
	//  or to '\n' character.
	uint64_t commentFinal = returnIdx - 1;

	assert(poundIdx <= commentFinal);

	if (commentFinal > poundIdx) {
		if (buf[commentFinal] == '\n') {
			commentFinal -= 1;
		}
	}

	if (commentFinal > poundIdx) {
		if (buf[commentFinal] == '\r') {
			commentFinal -= 1;
		}
	}

	assert(poundIdx <= commentFinal);

	if (commentFinal == poundIdx) {
		// Output empty comment
		IO_FPRINTF(outFile, "\t//\n");
	}
	else {
		assert(poundIdx < commentFinal);

		IO_FPRINTF(outFile, "\t//");

		// The comment is an untrusted string.
		// We avoid specifier (e.g. %d) issues by outputting
		//  one octet at a time.
		for (uint64_t i = poundIdx + 1; i <= commentFinal; i += 1) {
			IO_FPUTC(buf[i], outFile);
		}

		IO_FPUTC('\n', outFile);
	}

	return returnIdx;
}

// Handle "non-functional" parts of file.
// Step past comments and whitespace.
// Write out the comments if in the right section.
// Returns `start` if that index has neither whitespace nor starts a comment.
/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	requires 0 <= start <= bufLen;
	ensures start <= \result <= bufLen;
*/
uint64_t HandleNF(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t start,
	FILE *const outFile,
	enum Section currentSection)
{
	uint64_t currIdx = start;

	/*@
		loop invariant start <= currIdx <= bufLen;
		loop assigns currIdx;
		loop variant bufLen - currIdx;
	*/
	while (currIdx < bufLen) {
		if (IsWhitespace(buf[currIdx])) {
			currIdx += 1;
			continue;
		}

		if (buf[currIdx] == '#') {
			currIdx = HandleComment(buf, bufLen, currIdx, outFile, currentSection);
			continue;
		}

		break;
	}

	return currIdx;
}

// humanOctetIdx=1 means the first character of the corresponding line.
// humanOctetIdx must be on or before the first '\n'
//  that appears after the start of the line.
// "human" prefix means indexing starts at 1.
typedef struct FilePos {
	uint64_t humanLineNumber;
	uint64_t humanOctetIdx;
	uint64_t lineFirstIdx;
} FilePos;

/*@
	requires 0 <= givenIdx <= bufLen;
*/
FilePos FilePosFromIdx(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t givenIdx)
{
	assert(givenIdx >= 0 && givenIdx <= bufLen);

	uint64_t lineNumber = 1;
	uint64_t currentLineFirstIdx = 0;

	uint64_t idx = 0;

	while (idx < givenIdx) {
		if (buf[idx] == '\n') {
			lineNumber += 1;
			currentLineFirstIdx = idx + 1;
		}

		idx += 1;
	}

	FilePos pos = {
		.humanLineNumber = lineNumber,
		.humanOctetIdx = givenIdx - currentLineFirstIdx + 1,
		.lineFirstIdx = currentLineFirstIdx
	};

	return pos;
}

// Output the noted line with a caret underneath pointing to the noted character.
void OutputDebugLine(
	const uint8_t *const buf,
	const uint64_t bufLen,
	FILE *const f,
	FilePos pos)
{
	uint64_t idx = pos.lineFirstIdx;

	// ASSUME: '\r' is always followed by '\n'
	// Must check (idx < bufLen) since file may end without newline at end.
	while (idx < bufLen && buf[idx] != '\n' && buf[idx] != '\r') {
		IO_FPRINTF(f, "%c", buf[idx]);
		idx += 1;
	}

	IO_FPRINTF(f, "\n");

	for (uint64_t i = 1; i < pos.humanOctetIdx; i += 1) {
		IO_FPRINTF(f, " ");
	}

	IO_FPRINTF(f, "^\n");
}

/*@ predicate IntegerIsHexDigit(integer d) =
	((d >= '0') && (d <= '9')) ||
	((d >= 'a') && (d <= 'f')) ||
	((d >= 'A') && (d <= 'F'));
*/

// Return true if the given character is a hex digit, else return false.
/*@
	assigns \nothing;
	ensures \result <==> IntegerIsHexDigit(d);
*/
bool IsHexDigit(const uint8_t d) {
	return
		((d >= '0') && (d <= '9')) ||
		((d >= 'a') && (d <= 'f')) ||
		((d >= 'A') && (d <= 'F'));
}

// Return true if d is a valid character to follow the "0x" of a hex string,
//  one of [0-9], [a-f], [A-F], or '_'
// Else return false.
/*@
	assigns \nothing;
	ensures \result <==> (IntegerIsHexDigit(d) || (d == '_'));
*/
bool IsHexAllowedChar(const uint8_t d) {
	return IsHexDigit(d) || (d == '_');
}

// Assume d is a valid hex digit.
/*@
	requires IntegerIsHexDigit(d);
	requires '9' < 'A' < 'F' < 'a' < 'f';
	assigns \nothing;
	ensures 0 <= \result <= 15;
*/
uint64_t ValueFromHexDigit(const uint8_t d) {
	if (d <= '9') {
		return d - '0';
	}
	else if (d <= 'F') {
		return d - 'A' + 10;
	}
	else {
		return d - 'a' + 10;
	}
}

bool IsDecimalDigit(const uint8_t d) {
	return (d >= '0') && (d <= '9');
}

bool IsDecimalAllowedChar(const uint8_t d) {
	return (d == '_') || IsDecimalDigit(d);
}

typedef struct NumIdx {
	uint64_t num;
	uint64_t idx;
} NumIdx;

// At start, whitespace and comments are passed over and handled.
// Value must start with a [0-9] digit,
//  or '-' if the value is a negative decimal number.
// Value may be hex and start with "0x".
// Underscores are ignored.
// For hex values, the underscores may come only after the "0x".
// The caller must check if the expected characters (probably whitespace or semicolon)
//  begin at the returned idx.
/*@
	ensures start <= \result.idx <= bufLen;
*/
NumIdx ParseIntegerValue(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t start,
	FILE *const outFile,
	enum Section currentSection)
{
	uint64_t idx = HandleNF(buf, bufLen, start, outFile, currentSection);

	if (idx == bufLen) {
		FilePos pos = FilePosFromIdx(buf, bufLen, start);
		fprintf(stderr, "Failed to parse number (reached end of file) "
			"when starting at line %" PRIu64 ":%" PRIu64 "\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		exit(1);
	}
	else if ((buf[idx] != '-') && (buf[idx] < '0' || buf[idx] > '9')) {
		FilePos pos = FilePosFromIdx(buf, bufLen, idx);
		fprintf(stderr, "%s: Expected [0-9] or - at line %"
			PRIu64 ":%" PRIu64 "\n",
			__func__,
			pos.humanLineNumber, pos.humanOctetIdx);
		exit(1);
	}

	if (buf[idx] == '0' && idx < (bufLen - 1) && buf[idx + 1] == 'x') {
		if (idx == (bufLen - 2)) {
			fprintf(stderr, "Incomplete hex value at end of file.\n");
			exit(1);
		}

		// Index of the 'x' in the "0x".
		const uint64_t xIdx = idx + 1;

		// Step past '0' and 'x'.
		idx += 2;

		uint64_t value = 0;
		uint64_t numDigits = 0;

		while (idx < bufLen && IsHexAllowedChar(buf[idx])) {
			if (buf[idx] == '_') {
				idx += 1;
				continue;
			}

			value = (value << 4);
			value += ValueFromHexDigit(buf[idx]);

			numDigits += 1;

			idx += 1;
		}

		if (numDigits > 16) {
			FilePos pos = FilePosFromIdx(buf, bufLen, xIdx);
			fprintf(stderr, "Too many hex digits (%" PRIu64 ") following the "
				"\"0x\" at line %" PRIu64 ":%" PRIu64 "\n",
				numDigits, pos.humanLineNumber, pos.humanOctetIdx);
			OutputDebugLine(buf, bufLen, stderr, pos);
			exit(1);
		}

		const NumIdx retStruct = { .num = value, .idx = idx };
		return retStruct;
	}

	// At this point, value must be a pos or neg decimal integer.

	const bool isNegative = (buf[idx] == '-');
	if (isNegative) {
		idx += 1;
	}

	uint64_t value = 0;

	while (idx < bufLen && IsDecimalAllowedChar(buf[idx])) {
		// Declaring here due to use of goto.
		FilePos pos;

		if (buf[idx] == '_') {
			idx += 1;
			continue;
		}

		if (value > (UINT64_MAX / 10)) {
			goto Overflow;
		}

		value *= 10;

		const uint64_t newAddition = buf[idx] - '0';
		if (value > (UINT64_MAX - newAddition)) {
			goto Overflow;
		}

		value += newAddition;
		idx += 1;
		continue;

		Overflow:
		pos = FilePosFromIdx(buf, bufLen, idx);
		fprintf(stderr, "Digit at line %" PRIu64 ":%" PRIu64
			" causes value to overflow U64.\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		OutputDebugLine(buf, bufLen, stderr, pos);
		exit(1);
	}

	if (!isNegative) {
		const NumIdx retStruct = { .num = value, .idx = idx };
		return retStruct;
	}

	const uint64_t absI64Min = -((uint64_t)(INT64_MIN));

	if (value > absI64Min) {
		FilePos pos = FilePosFromIdx(buf, bufLen, idx);
		fprintf(stderr, "Number preceding line %" PRIu64 ":%" PRIu64
			" is too negative to fit in I64.\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		OutputDebugLine(buf, bufLen, stderr, pos);
		exit(1);
	}

	const int64_t i64Value = -value;
	// Casts back to uint64_t.
	const NumIdx retStruct = { .num = i64Value, .idx = idx };
	return retStruct;
}

// Parse non-negative, decimal integer.
// Permit leading zeros and leading underscores.
// At the start, whitespace and comments are passed over and handled.
// On the returned NumIdx,
//  num is the parsed number and idx is the index following what was parsed.
// Caller must determine if expected content (probably whitespace or semicolon)
//  begins at the returned idx.
NumIdx ParseNonNegDecInt(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t start,
	FILE *const outFile,
	enum Section currentSection)
{
	uint64_t idx = HandleNF(buf, bufLen, start, outFile, currentSection);

	if (idx == bufLen) {
		FilePos pos = FilePosFromIdx(buf, bufLen, start);
		fprintf(stderr, "Failed to parse non-negative decimal integer "
			"(reached end of file) when starting at line %"
			PRIu64 ":%" PRIu64 "\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		exit(1);
	}
	else if (buf[idx] < '0' || buf[idx] > '9') {
		FilePos pos = FilePosFromIdx(buf, bufLen, idx);
		fprintf(stderr, "Expected [0-9] at line %"
			PRIu64 ":%" PRIu64 "\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		exit(1);
	}

	uint64_t value = 0;

	while (idx < bufLen) {
		// Declare at beginning due to use of goto.
		FilePos pos;

		if (buf[idx] >= '0' && buf[idx] <= '9') {
			if (value > (UINT64_MAX / 10)) {
				goto Overflow;
			}

			value *= 10;

			const uint64_t newAddition = buf[idx] - '0';
			if (value > (UINT64_MAX - newAddition)) {
				goto Overflow;
			}

			value += newAddition;
			idx += 1;
			continue;
		}
		else if (buf[idx] == '_') {
			idx += 1;
			continue;
		}
		else {
			break;
		}

		Overflow:
		pos = FilePosFromIdx(buf, bufLen, idx);
		fprintf(stderr, "%s: Digit at line %" PRIu64 ":%" PRIu64
			" causes value to overflow U64.\n",
			__func__, pos.humanLineNumber, pos.humanOctetIdx);
		OutputDebugLine(buf, bufLen, stderr, pos);
		exit(1);
	}

	return (NumIdx){.num = value, .idx = idx};
}

typedef struct Data {
	uint8_t *buf;
	uint64_t length;
	uint64_t capacity;
} Data;

// Append given octet to buf of given Data.
// If data is already full, print to stderr and exit.
void Data_Append(Data *const data, const uint8_t c) {
	if (data->length == data->capacity) {
		fprintf(stderr, "Cannot append %c to full Data\n", c);
		exit(1);
	}

	data->buf[data->length] = c;
	data->length += 1;
}

bool IsIdentChar(const uint8_t c) {
	return
		((c >= 'a') && (c <= 'z')) ||
		((c >= 'A') && (c <= 'Z')) ||
		((c >= '0') && (c <= '9')) ||
		(c == '_');
}

// "Identifiers" are not permitted to start with a number.
bool IsLeadingIdentChar(const uint8_t c) {
	return
		((c >= 'a') && (c <= 'z')) ||
		((c >= 'A') && (c <= 'Z')) ||
		(c == '_');
}

// Requires that firstIdx is < bufLen
//  and the char at firstIdx is a valid ident character.
/*@
	ensures firstIdx <= \result < bufLen;
*/
uint64_t GetFinalIdentIdx(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t firstIdx)
{
	uint64_t idx = firstIdx;

	while (idx < (bufLen - 1)) {
		if (IsIdentChar(buf[idx + 1])) {
			idx += 1;
		}
		else {
			break;
		}
	}

	return idx;
}

// If the buf is not a semicolon at the given idx,
//  print to stderr and exit.
void ExpectSemicolonAfterCommand(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t idx)
{
	if (idx == bufLen) {
		fprintf(stderr,
			"Expected semicolon after command but reached end of file.\n");
		exit(1);
	}

	if (buf[idx] != ';') {
		FilePos pos = FilePosFromIdx(buf, bufLen, idx);
		fprintf(stderr, "Expected semicolon after command at line %"
			PRIu64 ":%" PRIu64 "\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		OutputDebugLine(buf, bufLen, stderr, pos);
		exit(1);
	}
}

uint64_t HandleCommand(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t dotIdx,
	FILE *const outFile,
	Data *const data,
	enum Section *const currentSection,
	bool *const enteredCodeSection)
{
	const uint64_t firstIdentIdx = dotIdx + 1;

	if (firstIdentIdx == bufLen) {
		fprintf(stderr, "Missing command at end of file.\n");
		exit(1);
	}

	if (!IsLeadingIdentChar(buf[firstIdentIdx])) {
		FilePos pos = FilePosFromIdx(buf, bufLen, firstIdentIdx);
		fprintf(stderr, "Expected command identifer characters "
			"(underscore or letters) at line %" PRIu64 ":%" PRIu64
			" but found: %c\n",
			pos.humanLineNumber, pos.humanOctetIdx, buf[firstIdentIdx]);
		OutputDebugLine(buf, bufLen, stderr, pos);
		exit(1);
	}

	const uint64_t finalIdentIdx = GetFinalIdentIdx(buf, bufLen, firstIdentIdx);
	const uint64_t identLen = finalIdentIdx - firstIdentIdx + 1;

	if (strncmp("data", &buf[firstIdentIdx], identLen) == 0) {
		if (*enteredCodeSection) {
			fprintf(stderr, "Must put data section before code section.\n");
			exit(1);
		}

		*currentSection = SECTION_DATA;
		const uint64_t nextIdx = finalIdentIdx + 1;

		ExpectSemicolonAfterCommand(buf, bufLen, nextIdx);

		return nextIdx + 1;
	}
	else if (strncmp("code", &buf[firstIdentIdx], identLen) == 0) {
		*currentSection = SECTION_CODE;
		*enteredCodeSection = true;
		const uint64_t nextIdx = finalIdentIdx + 1;

		ExpectSemicolonAfterCommand(buf, bufLen, nextIdx);

		return nextIdx + 1;
	}
	else if (strncmp("stringz", &buf[firstIdentIdx], identLen) == 0) {
		const uint64_t start = finalIdentIdx + 1;

		uint64_t idx = HandleNF(buf, bufLen, start, outFile, *currentSection);

		if (idx == bufLen) {
			fprintf(stderr, "Expected a string following .stringz "
				"but reached end of file.\n");
			exit(1);
		}
		else if (buf[idx] != '"') {
			FilePos pos = FilePosFromIdx(buf, bufLen, idx);
			fprintf(stderr, "Expected a string following .stringz "
				"but found character: %c at line %"
				PRIu64 ":%" PRIu64 "\n",
				buf[idx], pos.humanLineNumber, pos.humanOctetIdx);
			OutputDebugLine(buf, bufLen, stderr, pos);
			exit(1);
		}

		bool finishedString = false;
		idx += 1;
		while (idx < bufLen) {
			if (buf[idx] == '"') {
				finishedString = true;
				idx += 1;
				break;
			}
			else if (buf[idx] == '\\') {
				if (idx + 1 == bufLen) {
					break;
				}

				switch (buf[idx + 1]) {
					case 'r':
						Data_Append(data, '\r');
						break;
					case 'n':
						Data_Append(data, '\n');
						break;
					case 't':
						Data_Append(data, '\t');
						break;
					case '"':
						Data_Append(data, '"');
						break;
					default: {
						FilePos pos = FilePosFromIdx(buf, bufLen, idx + 1);
						fprintf(stderr, "Unknown escape \\%c at line %"
							PRIu64 ":%" PRIu64 "\n",
							buf[idx + 1], pos.humanLineNumber,
							pos.humanOctetIdx);
						OutputDebugLine(buf, bufLen, stderr, pos);
						exit(1);
					}
				}

				idx += 2;
				continue;
			}
			else {
				Data_Append(data, buf[idx]);
				idx += 1;
				continue;
			}
		}

		// Null terminate no matter if string finished or not.
		Data_Append(data, '\0');

		if (!finishedString) {
			fprintf(stderr, "Incomplete string at end of file.\n");
			exit(1);
		}

		// At this point, idx is the octet after the closing quote.

		idx = HandleNF(buf, bufLen, idx, outFile, *currentSection);

		ExpectSemicolonAfterCommand(buf, bufLen, idx);

		// Return position after the semicolon.
		return idx + 1;
	}
	else {
		FilePos pos = FilePosFromIdx(buf, bufLen, firstIdentIdx);
		fprintf(stderr, "Unknown command at line %" PRIu64 ":%" PRIu64 "\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		OutputDebugLine(buf, bufLen, stderr, pos);
		exit(1);
	}
}

// Write to outFile the label having the given length which begins
//  at the given idx in the given buf.
void WriteLabel(
	const uint8_t *const fileBuf,
	const uint64_t fileLen,
	FILE *const outFile,
	const uint64_t labelFirstIdx,
	const uint64_t labelLen)
{
	const size_t numWritten = fwrite(
		&fileBuf[labelFirstIdx], sizeof(fileBuf[0]), labelLen, outFile);

	if (numWritten != labelLen) {
		FilePos pos = FilePosFromIdx(fileBuf, fileLen, labelFirstIdx);
		fprintf(stderr, "Failed to write out the label starting at line %"
			PRIu64 ":%" PRIu64 "\n",
			pos.humanLineNumber, pos.humanOctetIdx);
		OutputDebugLine(fileBuf, fileLen, stderr, pos);
		exit(1);
	}
}

/*@
	assigns \nothing;
*/
int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Pass a single path to a .asm file\n");
		exit(1);
	}

	uint8_t *const inputFilePath = argv[1];

	uint8_t *fileBuf;
	uint64_t fileLen;

	io_ReadEntireFile(inputFilePath, &fileBuf, &fileLen);

	char *const initialBoilerplate =
"#include <stdbool.h>\n"
"#include <inttypes.h>\n"
"#include <stdint.h>\n"
"#include <stdio.h>\n"
"#include <stdlib.h>\n"
"#include <time.h>\n"
"\n"
"void *mem_Alloc(const size_t size) {\n"
"\tvoid *const ptr = malloc(size);\n"
"\n"
"\tif (ptr == NULL && size != 0) {\n"
"\t\tfprintf(stderr, \"%s: Failed to malloc %zu\\n\", __func__, size);\n"
"\t\texit(1);\n"
"}\n"
"\n"
"\treturn ptr;\n"
"}\n"
"\n"
"void *mem_Realloc(void *const ptr, const size_t newSize) {\n"
"\tvoid *const newPtr = realloc(ptr, newSize);\n"
"\n"
"\tif (newPtr == NULL && newSize != 0) {\n"
"\t\tfprintf(stderr, \"%s: Failed to realloc pointer %p to %zu bytes\\n\",\n"
"\t\t\t__func__, ptr, newSize);\n"
"\t\texit(1);\n"
"\t}\n"
"\n"
"\treturn newPtr;\n"
"}\n"
"\n"
"uint64_t clock_GetTimeMs(void) {\n"
"\tstruct timespec ts;\n"
"\n"
"\tif (timespec_get(&ts, TIME_UTC) == 0) {\n"
"\t\tfprintf(stderr, \"%s: timespec_get error\\n\", __func__);\n"
"\t\texit(1);\n"
"\t}\n"
"\n"
"\treturn ((uint64_t)ts.tv_sec) * UINT64_C(1000) + ((uint64_t)ts.tv_nsec) / UINT64_C(1000000);\n"
"}\n"
"\n"
"bool IsDigit(const int c) {\n"
"\treturn (c >= '0') && (c <= '9');\n"
"}\n"
"\n"
"int64_t I64FromU64(const uint64_t in) {\n"
"\tconst int64_t out = in;\n"
"\treturn out;\n"
"}\n"
"\n"
"uint64_t U64FromI64(const int64_t in) {\n"
"\tconst uint64_t out = in;\n"
"\treturn out;\n"
"}\n"
"\n"
"typedef struct Stack {\n"
"\tuint64_t *buf;\n"
"\tuint64_t size;\n"
"\tuint64_t cap;\n"
"} Stack;\n"
"\n"
"void Stack_Init(Stack *const stack) {\n"
"\tstack->cap = 1024;\n"
"\tstack->buf = mem_Alloc(sizeof(stack->buf[0]) * stack->cap);\n"
"\tstack->size = 0;\n"
"}\n"
"\n"
"void Stack_Push(Stack *const stack, const uint64_t value) {\n"
"\tif (stack->size == stack->cap) {\n"
"\t\tfprintf(stderr, \"Stack is full.\\n\");\n"
"\t}\n"
"\n"
"\tstack->buf[stack->size] = value;\n"
"\tstack->size += 1;\n"
"}\n"
"\n"
"void Stack_PushI64(Stack *const stack, const int64_t ivalue) {\n"
"\tconst uint64_t uv = U64FromI64(ivalue);\n"
"\tStack_Push(stack, uv);\n"
"}\n"
"\n"
"uint64_t Stack_Pop(Stack *const stack) {\n"
"\tif (stack->size == 0) {\n"
"\t\tfputs(\"tried to pop when the stack is empty\\n\", stderr);\n"
"\t}\n"
"\n"
"\tstack->size -= 1;\n"
"\tconst uint64_t value = stack->buf[stack->size];\n"
"\treturn value;\n"
"}\n"
"\n"
"int64_t Stack_PopI64(Stack *const stack) {\n"
"\tconst uint64_t uvalue = Stack_Pop(stack);\n"
"\tconst int64_t out = I64FromU64(uvalue);\n"
"\treturn out;\n"
"}\n"
"\n"
"typedef struct Frame {\n"
"\tuint64_t baseIdx;\n"
"\tuint64_t callingSiteNum;\n"
"\tuint64_t argc;\n"
"} Frame;\n"
"\n"
"typedef struct FrameStack {\n"
"\tFrame *buf;\n"
"\tuint64_t size;\n"
"\tuint64_t cap;\n"
"} FrameStack;\n"
"\n"
"void FrameStack_Init(FrameStack *const stack) {\n"
"\tstack->cap = 1024;\n"
"\tstack->buf = mem_Alloc(sizeof(stack->buf[0]) * stack->cap);\n"
"\tstack->size = 0;\n"
"}\n"
"\n"
"void FrameStack_Push(FrameStack *const stack, Frame frame) {\n"
"\tif (stack->size == stack->cap) {\n"
"\t\tfprintf(stderr, \"Function call stack is full.\\n\");\n"
"\t}\n"
"\n"
"\tstack->buf[stack->size] = frame;\n"
"\tstack->size += 1;\n"
"}\n"
"\n"
"Frame FrameStack_Pop(FrameStack *const stack) {\n"
"\tif (stack->size == 0) {\n"
"\t\tfputs(\"tried to pop when the frame stack is empty\\n\", stderr);\n"
"\t\texit(1);\n"
"\t}\n"
"\n"
"\tstack->size -= 1;\n"
"\tconst Frame retFrame = stack->buf[stack->size];\n"
"\treturn retFrame;\n"
"}\n"
"\n"
"int main(void) {\n"
"\tStack stack;\n"
"\tStack_Init(&stack);\n"
"\n"
"\tFrameStack frameStack;\n"
"\tFrameStack_Init(&frameStack);\n"
"\tFrameStack_Push(&frameStack, (Frame){.baseIdx=0,.callingSiteNum=0,.argc=0});\n"
"\n"
"\tconst uint8_t *dataArr;\n"   // We do this weird goto with a goto back shortly after
"\tgoto tr__PopulateDataArr;\n" //  so that we can handle the input in one pass.
"\ttr__AfterPopulateDataArr:\n" // We need to output the beginning of the output file
"\n"                            //  but won't have the value for dataArr until later.
"";

	FILE *const outFile = stdout;

	IO_FPUTS(initialBoilerplate, outFile);

	enum Section currentSection = SECTION_CODE;
	// We will require that the .data section, if present, comes before .code section.
	bool enteredCodeSection = false;

	#define DATA_BUF_CAP (64 * 1024)
	uint8_t dataBuf[DATA_BUF_CAP];
	Data data = {.buf = dataBuf, .length = 0, .capacity = DATA_BUF_CAP};
	#define DATA_LABELS_CAP (1024)
	char *dataLabels[DATA_LABELS_CAP];
	uint64_t numDataLabels = 0;
	uint64_t dataLabelOffset[DATA_LABELS_CAP];

	#define CODE_LABELS_CAP (1024)
	char *codeLabels[CODE_LABELS_CAP];
	uint64_t numCodeLabels;

	// Call site 0 is the "caller" of the initial frame.
	// Call site 0 should never be jumped to.
	uint64_t numCallSites = 1;

	uint64_t idx = 0;
	while (idx < fileLen) {
		idx = HandleNF(fileBuf, fileLen, idx, outFile, currentSection);

		if (idx == fileLen) {
			break;
		}

		if (fileBuf[idx] == '.') {
			idx = HandleCommand(fileBuf, fileLen, idx, outFile,
				&data, &currentSection, &enteredCodeSection);

			continue;
		}

		if (!IsLeadingIdentChar(fileBuf[idx])) {
			FilePos pos = FilePosFromIdx(fileBuf, fileLen, idx);
			fprintf(stderr, "Expected letter or underscore at "
				"line %" PRIu64 ":%" PRIu64 "\n",
				pos.humanLineNumber, pos.humanOctetIdx);
			OutputDebugLine(fileBuf, fileLen, stderr, pos);
			exit(1);
		}

		const uint64_t firstIdentIdx = idx;
		const uint64_t finalIdentIdx = GetFinalIdentIdx(fileBuf, fileLen, firstIdentIdx);
		const uint64_t identLen = finalIdentIdx - idx + 1;

		// ASSUME: No whitespace between label names and their colon.
		if (finalIdentIdx < fileLen - 1 && fileBuf[finalIdentIdx + 1] == ':') {
			// Handle label.

			char *const ident = mem_Alloc(sizeof(ident[0]) * identLen + 1);
			memcpy(ident, &fileBuf[idx], identLen);
			ident[identLen] = '\0';

			if (currentSection == SECTION_DATA) {
				if (numDataLabels == DATA_LABELS_CAP) {
					fprintf(stderr, "Too many labels in the .data section.\n");
					exit(1);
				}

				dataLabelOffset[numDataLabels] = data.length;
				dataLabels[numDataLabels] = ident;
				numDataLabels += 1;
			}
			else {
				assert(currentSection == SECTION_CODE);

				if (numCodeLabels == CODE_LABELS_CAP) {
					fprintf(stderr, "Too many labels in the .code section.\n");
					exit(1);
				}

				codeLabels[numCodeLabels] = ident;
				numCodeLabels += 1;

				// ASSUME: Label has valid characters to be a C label.
				fprintf(outFile, "\t%s:\n", codeLabels[numCodeLabels - 1]);
			}

			// Step past ':'
			idx = finalIdentIdx + 2;
			continue;
		} // End of block that handles labels.

		// Handle instruction.

		if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "push")) {
			idx = finalIdentIdx + 1;
			idx = HandleNF(fileBuf, fileLen, idx, outFile, currentSection);

			if (IsLeadingIdentChar(fileBuf[idx])) {
				// Parse a label name.

				const uint64_t lnameFirstIdx = idx;
				const uint64_t lnameFinalIdx = GetFinalIdentIdx(fileBuf, fileLen, lnameFirstIdx);
				const uint64_t lnameLen = lnameFinalIdx - lnameFirstIdx + 1;

				bool foundLabel = false;
				uint64_t dataLabelIdx;

				for (uint64_t i = 0; i < numDataLabels; i += 1) {
					if (SliceMatchesString(fileBuf, lnameFirstIdx, lnameLen, dataLabels[i])) {
						foundLabel = true;
						dataLabelIdx = i;
						break;
					}
				}

				if (!foundLabel) {
					FilePos pos = FilePosFromIdx(fileBuf, fileLen, lnameFirstIdx);
					fprintf(stderr, "Unknown label being pushed at line %" PRIu64
						":%" PRIu64 "\n", pos.humanLineNumber, pos.humanOctetIdx);
					OutputDebugLine(fileBuf, fileLen, stderr, pos);
					exit(1);
				}

				const uint64_t offset = dataLabelOffset[dataLabelIdx];

				IO_FPRINTF(outFile,
					"\t// push\n"
					"\tStack_Push(&stack, UINT64_C(%" PRIu64 "));\n"
					"\n", offset);

				idx = lnameFinalIdx + 1;
			}
			else {
				NumIdx numIdx = ParseIntegerValue(fileBuf, fileLen,
						finalIdentIdx + 1, outFile, currentSection);

				IO_FPRINTF(outFile,
					"\t// push\n"
					"\tStack_Push(&stack, UINT64_C(%" PRIu64 "));\n"
					"\n", numIdx.num);

				idx = numIdx.idx;
			}
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "swap")) {
			IO_FPUTS(
				"\t{ // swap\n"
				"\t\tconst uint64_t a = Stack_Pop(&stack);\n"
				"\t\tconst uint64_t b = Stack_Pop(&stack);\n"
				"\n"
				"\t\tStack_Push(&stack, a);\n"
				"\t\tStack_Push(&stack, b);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "exit")) {
			IO_FPRINTF(outFile,
				"\t{ // exit\n"
				"\t\tFrame frame = frameStack.buf[frameStack.size - 1];\n"
				"\n"
				"\t\tif (stack.size <= frame.baseIdx) {\n"
				"\t\t\tfprintf(stderr, \"exit with no return value on stack\\n\");\n"
				"\t\t\texit(126);\n"
				"\t\t}\n"
				"\n"
				"\t\tconst int retVal = stack.buf[stack.size - 1];\n"
				"\t\texit(retVal);\n"
				"\t}\n"
				"\n"
			);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "get_local")) {
			NumIdx numIdx = ParseNonNegDecInt(fileBuf, fileLen,
				finalIdentIdx + 1, outFile, currentSection);

			IO_FPRINTF(outFile,
				"\t{ // get_local\n"
				"\t\tFrame frame = frameStack.buf[frameStack.size - 1];\n"
				"\n"
				"\t\tconst uint64_t idx = frame.baseIdx + %" PRIu64 ";\n"
				"\n"
				"\t\tif (idx >= stack.size) {\n"
				"\t\t\tfprintf(stderr, \"invalid index %%\" PRIu64 "
					"\" in get_local\\n\", idx);\n"
				"\t\t\texit(1);\n"
				"\t\t}\n"
				"\n"
				"\t\tStack_Push(&stack, stack.buf[idx]);\n"
				"\t}\n"
				"\n",
				numIdx.num
			);

			idx = numIdx.idx;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "set_local")) {
			NumIdx numIdx = ParseNonNegDecInt(fileBuf, fileLen,
				finalIdentIdx + 1, outFile, currentSection);

			IO_FPRINTF(outFile,
				"\t{ // set_local\n"
				"\t\tFrame frame = frameStack.buf[frameStack.size - 1];\n"
				"\n"
				"\t\tconst uint64_t idx = frame.baseIdx + %" PRIu64 ";\n"
				"\t\tconst uint64_t val = Stack_Pop(&stack);\n"
				"\n"
				"\t\tif (idx >= stack.size) {\n"
				"\t\t\tfprintf(stderr, \"invalid index %%\" PRIu64 "
					"\" in set_local\\n\", idx);\n"
				"\t\t\texit(1);\n"
				"\t\t}\n"
				"\n"
				"\t\tstack.buf[idx] = val;\n"
				"\t}\n"
				"\n",
				numIdx.num
			);

			idx = numIdx.idx;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "get_arg")) {
			NumIdx numIdx = ParseNonNegDecInt(fileBuf, fileLen,
				finalIdentIdx + 1, outFile, currentSection);

			IO_FPRINTF(outFile,
				"\t{ // get_arg\n"
				"\t\tFrame frame = frameStack.buf[frameStack.size - 1];\n"
				"\t\tconst uint64_t idx = %" PRIu64 ";\n"
				"\n"
				"\t\tif (idx >= frame.argc) {\n"
				"\t\t\tfprintf(stderr, \"invalid index in get_arg, idx=%%\" PRIu64 \""
					", argc=%%\" PRIu64 \"\\n\",\n"
				"\t\t\t\tidx, frame.argc);\n"
				"\t\t\texit(1);\n"
				"\t\t}\n"
				"\n"
				"\t\tconst uint64_t stackIdx = (frame.baseIdx - frame.argc) + idx;\n"
				"\n"
				"\t\tif (stackIdx >= stack.size) {\n"
				"\t\t\tfprintf(stderr, \"invalid index %%\" PRIu64 "
					"\"\\n\", stackIdx);\n"
				"\t\t\texit(1);\n"
				"\t\t}\n"
				"\n"
				"\t\tStack_Push(&stack, stack.buf[stackIdx]);\n"
				"\t}\n"
				"\n",
				numIdx.num
			);

			idx = numIdx.idx;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "lt_i64")) {
			IO_FPUTS(
				"\t{ // lt_i64\n"
				"\t\tconst int64_t v1 = Stack_PopI64(&stack);\n"
				"\t\tconst int64_t v0 = Stack_PopI64(&stack);\n"
				"\n"
				"\t\tconst uint64_t compare = (v0 < v1);\n"
				"\t\tStack_Push(&stack, compare);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "add_u64")) {
			IO_FPUTS(
				"\t{ // add_u64\n"
				"\t\tconst uint64_t v1 = Stack_Pop(&stack);\n"
				"\t\tconst uint64_t v0 = Stack_Pop(&stack);\n"
				"\n"
				"\t\tconst uint64_t sum = (v0 + v1);\n"
				"\t\tStack_Push(&stack, sum);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "sub_u64")) {
			IO_FPUTS(
				"\t{ // sub_u64\n"
				"\t\tconst uint64_t v1 = Stack_Pop(&stack);\n"
				"\t\tconst uint64_t v0 = Stack_Pop(&stack);\n"
				"\n"
				"\t\tconst uint64_t result = (v0 - v1);\n"
				"\t\tStack_Push(&stack, result);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "mul_u64")) {
			IO_FPUTS(
				"\t{ // mul_u64\n"
				"\t\tconst uint64_t v1 = Stack_Pop(&stack);\n"
				"\t\tconst uint64_t v0 = Stack_Pop(&stack);\n"
				"\n"
				"\t\tconst uint64_t result = (v0 * v1);\n"
				"\t\tStack_Push(&stack, result);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "div_u64")) {
			IO_FPUTS(
				"\t{ // div_u64\n"
				"\t\tconst uint64_t v1 = Stack_Pop(&stack);\n"
				"\t\tconst uint64_t v0 = Stack_Pop(&stack);\n"
				"\n"
				"\t\tconst uint64_t result = (v0 / v1);\n"
				"\t\tStack_Push(&stack, result);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "div_i64")) {
			IO_FPUTS(
				"\t{ // div_i64\n"
				"\t\tconst int64_t v1 = Stack_PopI64(&stack);\n"
				"\t\tconst int64_t v0 = Stack_PopI64(&stack);\n"
				"\n"
				"\t\tconst int64_t result = (v0 / v1);\n"
				"\t\tStack_PushI64(&stack, result);\n"
				"\t}\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "jmp") ||
		         SliceMatchesString(fileBuf, firstIdentIdx, identLen, "jz" ) ||
		         SliceMatchesString(fileBuf, firstIdentIdx, identLen, "jnz"))
		{
			idx = finalIdentIdx + 1;
			idx = HandleNF(fileBuf, fileLen, idx, outFile, currentSection);

			const uint64_t labelFirstIdx = idx;

			if (idx == fileLen) {
				fprintf(stderr, "Expected label name but reached end of file.\n");
				exit(1);
			}
			else if (!IsLeadingIdentChar(fileBuf[idx])) {
				FilePos pos = FilePosFromIdx(fileBuf, fileLen, idx);
				fprintf(stderr, "Expected first character of a label "
					"but found: %c at line %" PRIu64 ":%" PRIu64 "\n",
					fileBuf[idx],
					pos.humanLineNumber, pos.humanOctetIdx);
				OutputDebugLine(fileBuf, fileLen, stderr, pos);
				exit(1);
			}

			const uint64_t labelFinalIdx = GetFinalIdentIdx(
				fileBuf, fileLen, labelFirstIdx);
			const uint64_t labelLen = labelFinalIdx - labelFirstIdx + 1;

			// ASSUME: Label name is valid.

			switch(fileBuf[firstIdentIdx + 1]) {
				case 'm': // jmp
				{
					IO_FPUTS(
						"\t// jmp\n"
						"\tgoto ",
						outFile);

					WriteLabel(fileBuf, fileLen, outFile, labelFirstIdx, labelLen);

					IO_FPUTS(";\n\n", outFile);
				} break;
				case 'z': // jz
				{
					IO_FPUTS(
						"\t{ // jz\n"
						"\t\tconst uint64_t value = Stack_Pop(&stack);\n"
						"\t\tconst bool shouldJump = (value == 0);\n"
						"\n"
						"\t\tif (shouldJump) {\n"
						"\t\t\tgoto ",
						outFile);

					WriteLabel(fileBuf, fileLen, outFile, labelFirstIdx, labelLen);

					IO_FPUTS(
						";\n"
						"\t\t}\n"
						"\t}\n"
						"\n",
						outFile);
				} break;
				case 'n': // jnz
				{
					IO_FPUTS(
						"\t{ // jnz\n"
						"\t\tconst uint64_t value = Stack_Pop(&stack);\n"
						"\t\tconst bool shouldJump = (value != 0);\n"
						"\n"
						"\t\tif (shouldJump) {\n"
						"\t\t\tgoto ",
						outFile);

					WriteLabel(fileBuf, fileLen, outFile, labelFirstIdx, labelLen);

					IO_FPUTS(
						";\n"
						"\t\t}\n"
						"\t}\n"
						"\n",
						outFile);
				} break;
				default:
				{
					fprintf(stderr, "Unreachable jmp switch.\n");
					exit(1);
				}
			}

			idx = labelFinalIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "ret")) {
			IO_FPUTS(
				"\t// ret\n"
				"\tgoto tr__RetRouter;\n"
				"\n",
				outFile);

			idx = finalIdentIdx + 1;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "call")) {
			idx = HandleNF(fileBuf, fileLen, finalIdentIdx + 1, outFile, currentSection);

			if (idx == fileLen) {
				fprintf(stderr, "Expected args to a call instruction but reached EOF.\n");
				exit(1);
			}
			else if (!IsLeadingIdentChar(fileBuf[idx])) {
				fprintf(stderr, "Expected label name after call instruction.\n");
				exit(1);
			}

			const uint64_t lnameFirstIdx = idx;
			const uint64_t lnameFinalIdx = GetFinalIdentIdx(fileBuf, fileLen, lnameFirstIdx);
			const uint64_t lnameLen = lnameFinalIdx - lnameFirstIdx + 1;

			idx = HandleNF(fileBuf, fileLen, lnameFinalIdx + 1, outFile, currentSection);

			if (idx == fileLen) {
				fprintf(stderr, "Expected comma but found EOF.\n");
				exit(1);
			}
			else if (fileBuf[idx] != ',') {
				fprintf(stderr, "Expected comma but found other char.\n");
				exit(1);
			}

			// Step past the comma.
			idx += 1;

			const NumIdx numIdx = ParseNonNegDecInt(fileBuf, fileLen, idx, outFile, currentSection);

			IO_FPRINTF(outFile,
				"\t{ // call\n"
				"\t\tconst Frame f = {\n"
				"\t\t\t.baseIdx = stack.size,\n"
				"\t\t\t.callingSiteNum = %" PRIu64 ",\n"
				"\t\t\t.argc = %" PRIu64 "\n"
				"\t\t};\n"
				"\n"
				"\t\tFrameStack_Push(&frameStack, f);\n"
				"\t\tgoto ",
				numCallSites, numIdx.num);

			const size_t numWritten = fwrite(&fileBuf[lnameFirstIdx],
				sizeof(fileBuf[0]), lnameLen, outFile);
			if (numWritten != lnameLen) {
				fprintf(stderr, "Error outputting label name.\n");
				exit(1);
			}

			IO_FPRINTF(outFile,
				";\n"
				"\t}\n"
				"\ttr__CallSite%" PRIu64 ":\n"
				"\n",
				numCallSites);

			numCallSites += 1;// ASSUME: No overflow.
			idx = numIdx.idx;
		}
		else if (SliceMatchesString(fileBuf, firstIdentIdx, identLen, "syscall")) {
			idx = finalIdentIdx + 1;
			idx = HandleNF(fileBuf, fileLen, idx, outFile, currentSection);

			const uint64_t snameFirstIdx = idx;

			if (idx == fileLen) {
				fprintf(stderr, "Expected syscall name but reached end of file.\n");
				exit(1);
			}
			else if (!IsLeadingIdentChar(fileBuf[idx])) {
				FilePos pos = FilePosFromIdx(fileBuf, fileLen, idx);
				fprintf(stderr, "Expected first character of a syscall name "
					"but found: %c at line %" PRIu64 ":%" PRIu64 "\n",
					fileBuf[idx],
					pos.humanLineNumber, pos.humanOctetIdx);
				OutputDebugLine(fileBuf, fileLen, stderr, pos);
				exit(1);
			}

			const uint64_t snameFinalIdx = GetFinalIdentIdx(
				fileBuf, fileLen, snameFirstIdx);
			const uint64_t snameLen = snameFinalIdx - snameFirstIdx + 1;

			if (SliceMatchesString(fileBuf, snameFirstIdx, snameLen, "time_current_ms")) {
				IO_FPUTS(
					"\t{ // syscall time_current_ms\n"
					"\t\tconst uint64_t timeMs = clock_GetTimeMs();\n"
					"\t\tStack_Push(&stack, timeMs);\n"
					"\t}\n"
					"\n",
					outFile);
			}
			else if (SliceMatchesString(fileBuf, snameFirstIdx, snameLen, "print_i64")) {
				IO_FPUTS(
					"\t{ // syscall print_i64\n"
					"\t\tconst int64_t ival = Stack_PopI64(&stack);\n"
					"\t\tprintf(\"%\" PRIi64, ival);\n"
					"\t}\n"
					"\n",
					outFile);
			}
			else if (SliceMatchesString(fileBuf, snameFirstIdx, snameLen, "print_endl")) {
				IO_FPUTS(
					"\t// syscall print_endl\n"
					"\tfputs(\"\\n\", stdout);\n"
					"\n",
					outFile);
			}
			else if (SliceMatchesString(fileBuf, snameFirstIdx, snameLen, "print_str")) {
				IO_FPUTS(
					"\t{ // syscall print_str\n"
					"\t\tconst uint64_t offset = Stack_Pop(&stack);\n"
					"\t\tprintf(\"%s\", (char*)&dataArr[offset]);\n"
					"\t}\n"
					"\n",
					outFile);
			}
			else if (SliceMatchesString(fileBuf, snameFirstIdx, snameLen, "read_i64")) {
				IO_FPUTS(
					"\t{ // syscall read_i64\n"
					"\t\tint in;\n"
					"\n"
					"\t\twhile (true) {\n"
					"\t\t\tin = fgetc(stdin);\n"
					"\n"
					"\t\t\tif (in == EOF) {\n"
					"\t\t\t\tfprintf(stderr, \"fgetc error\\n\");\n"
					"\t\t\t\texit(1);\n"
					"\t\t\t}\n"
					"\t\t\telse if (in == '\\n') {\n"
					"\t\t\t\tfprintf(stderr, \"read_i64 found no i64 on line.\\n\");\n"
					"\t\t\t\texit(1);\n"
					"\t\t\t}\n"
					"\t\t\telse if (in == ' ' || in == '\\r' || in == '\\t') {\n"
					"\t\t\t\tcontinue;\n"
					"\t\t\t}\n"
					"\t\t\telse if (IsDigit(in)) {\n"
					"\t\t\t\tbreak;\n"
					"\t\t\t}\n"
					"\n"
					"\t\t\tfprintf(stderr, \"Expected I64 but found: %c\\n\", in);\n"
					"\t\t\texit(1);\n"
					"\t\t}\n"
					"\n"
					"\t\tint64_t result = 0;\n"
					"\n"
					"\t\t// TODO: Support negative numbers.\n"
					"\n"
					"\t\twhile (IsDigit(in)) {\n"
					"\t\t\tif (result > INT64_MAX / 10) {\n"
					"\t\t\t\tfprintf(stderr, \"Input to read_i64 overflows INT64_MAX\\n\");\n"
					"\t\t\t\texit(1);\n"
					"\t\t\t}\n"
					"\n"
					"\t\t\tresult *= 10;\n"
					"\n"
					"\t\t\tconst int64_t add = in - '0';\n"
					"\n"
					"\t\t\tif (result > INT64_MAX - add) {\n"
					"\t\t\t\tfprintf(stderr, \"Input to read_i64 overflows INT64_MAX\\n\");\n"
					"\t\t\t\texit(1);\n"
					"\t\t\t}\n"
					"\n"
					"\t\t\tresult += add;\n"
					"\n"
					"\t\t\tin = fgetc(stdin);\n"
					"\n"
					"\t\t\tif (in == EOF) {\n"
					"\t\t\t\tfprintf(stderr, \"fgetc error\\n\");\n"
					"\t\t\t\texit(1);\n"
					"\t\t\t}\n"
					"\t\t}\n"
					"\n"
					"\t\twhile (in == ' ' || in == '\\r' || in == '\\n' || in == '\\t') {\n"
					"\t\t\tif (in == '\\n') {\n"
					"\t\t\t\tgoto Done;\n"
					"\t\t\t}\n"
					"\n"
					"\t\t\tin = fgetc(stdin);\n"
					"\n"
					"\t\t\tif (in == EOF) {\n"
					"\t\t\t\tfprintf(stderr, \"fgetc error\\n\");\n"
					"\t\t\t\texit(1);\n"
					"\t\t\t}\n"
					"\t\t}\n"
					"\n"
					"\t\tfprintf(stderr, \"Read I64 value of %\" PRIi64\n"
					"\t\t\t\" then found invalid char: %c\\n\",\n"
					"\t\t\tresult, in);\n"
					"\t\texit(1);\n"
					"\n"
					"\t\tDone:\n"
					"\t\tStack_PushI64(&stack, result);\n"
					"\t}\n"
					"\n",
					outFile
				);
			}
			else {
				// TODO: Handle more syscalls.

				FilePos pos = FilePosFromIdx(fileBuf, fileLen, snameFirstIdx);
				fprintf(stderr, "Unknown syscall name at line %" PRIu64 ":%"
					PRIu64 "\n",
					pos.humanLineNumber, pos.humanOctetIdx);
				OutputDebugLine(fileBuf, fileLen, stderr, pos);
				exit(1);
			}

			idx = snameFinalIdx + 1;
		}
		else {
			// TODO: Handle more instructions.

			FilePos pos = FilePosFromIdx(fileBuf, fileLen, firstIdentIdx);
			fprintf(stderr, "At line %" PRIu64 ":%" PRIu64
				", unknown instruction: ",
				pos.humanLineNumber, pos.humanOctetIdx);

			for (uint64_t c = 0; c < identLen; c += 1) {
				fputc(fileBuf[firstIdentIdx + c], stderr);
			}

			fputc('\n', stderr);

			exit(1);
		}

		idx = HandleNF(fileBuf, fileLen, idx, outFile, currentSection);

		if (idx == fileLen) {
			fprintf(stderr, "Expected semicolon after instruction but reached end of file.\n");
			exit(1);
		}

		if (fileBuf[idx] != ';') {
			FilePos pos = FilePosFromIdx(fileBuf, fileLen, idx);
			fprintf(stderr, "After an instruction, expected semicolon at line %"
				PRIu64 ":%" PRIu64 "\n",
				pos.humanLineNumber, pos.humanOctetIdx);
			OutputDebugLine(fileBuf, fileLen, stderr, pos);
			exit(1);
		}

		// Step past ';'
		idx += 1;
	} // End of while

	// If execution reaches end of .asm file:
	// Same error message as uvm.
	IO_FPUTS(
		"\tfputs(\"pc outside bounds of code space\\n\", stderr);\n"
		"\texit(1);\n"
		"\n",
		outFile);

	IO_FPUTS(
		"\ttr__PopulateDataArr:\n"
		"\tdataArr = (const uint8_t[]){\n",
		outFile);

	for (uint64_t i = 0; i < data.length;) {
		const uint64_t rowFirstIdx = i;

		IO_FPUTS("\t\t", outFile);

		for (; i < (rowFirstIdx + 17) && i < data.length; i += 1) {
			IO_FPRINTF(outFile, "%3" PRIu8 ",", data.buf[i]);
		}

		IO_FPUTS("\n", outFile);
	}

	// Add a null-terminator (zero) at end for no specific reason.
	// (Although it does solve the dangling comma situation.)
	IO_FPUTS(
		"\t\t0\n"
		"\t};\n" // End of dataArr value.
		"\n"
		"\t{ // Avoid unused-but-set warning.\n"
		"\t\tconst uint8_t *foo = dataArr;\n"
		"\t\tfoo = foo;\n"
		"\t}\n"
		"\tgoto tr__AfterPopulateDataArr;\n",
		outFile);

	if (numCallSites > 1) {
		IO_FPUTS(
			"\n"
			"\ttr__RetRouter:\n"
			"\t{\n"
			"\t\tconst Frame frame = FrameStack_Pop(&frameStack);\n"
			"\n"
			"\t\tif (stack.size <= frame.baseIdx) {\n"
			// Same error message as uvm.
			"\t\t\tfputs(\"ret with no return value on stack\\n\", stderr);\n"
			"\t\t\texit(1);\n"
			"\t\t}\n"
			"\n"
			"\t\tconst uint64_t retVal = Stack_Pop(&stack);\n"
			"\n"
			"\t\tif (frameStack.size == 0) { // If this is a top-level return:\n"
			"\t\t\texit(retVal);\n"
			"\t\t}\n"
			"\n"
			"\t\tstack.size = frame.baseIdx - frame.argc;\n"
			"\t\tStack_Push(&stack, retVal);\n"
			"\n"
			"\t\tswitch (frame.callingSiteNum) {\n",
			outFile);

		for (uint64_t i = 1; i < numCallSites; i += 1) {
			IO_FPRINTF(outFile, "\t\t\tcase %" PRIu64 ": goto tr__CallSite%"
				PRIu64 ";\n", i, i);
		}

		IO_FPUTS(
			"\t\t}\n" // Close the switch.
			"\n"
			"\t\t// Should be unreachable.\n"
			"\t\tfprintf(stderr, \"Invalid calling site number %\" PRIu64 \"\\n\",\n"
			"\t\t\tframe.callingSiteNum);\n"
			"\t\texit(1);\n"
			"\t}\n"
			"}\n", // Close main function.
			outFile);
	}
	else {
		// No need for the RetRouter.
		// Only need to close the main function.

		IO_FPUTS("}\n", outFile);
	}

	// Debug:
	//fprintf(stderr, "%s\n", data.buf);
	//fprintf(stderr, "numDataLabels: %" PRIu64 "\n", numDataLabels);
	//fprintf(stderr, "numCodeLabels: %" PRIu64 "\n", numCodeLabels);

	for (uint64_t i = 0; i < numDataLabels; i += 1) {
		free(dataLabels[i]);
	}

	for (uint64_t i = 0; i < numCodeLabels; i += 1) {
		free(codeLabels[i]);
	}

	free(fileBuf);
	return 0;
}

/*@
	assigns \nothing;
*/
int eva_main(void) {
	char *argv[3];
	argv[0] = "toc";
	argv[1] = "somefile.asm";
	argv[2] = NULL;

	return main(2, argv);
}
