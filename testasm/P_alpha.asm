BEGIN: # BEGIN
#ABC
#123
# Leading space.
#	Leading tab.
#

#

SECTION_A:
 SECTION_LEADING_SPACE:
	SECTION_LEADING_TAB:


      # abc
.data;# After section.
# 123
.stringz "Hello\n\tIndented second line";

.code;     # After section.

#a

push 3;
push 5  ;
push 4	;

push 0;
push 1;
lt_i64;

syscall time_current_ms;

jmp REST;
jz LABEL_JZ;
jnz LABEL_JNZ;

LABEL_JZ:
LABEL_JNZ:
REST:
get_local 0;
get_local 1;
add_u64;
get_local 2;
sub_u64;

syscall read_i64;

push 0;

exit;
