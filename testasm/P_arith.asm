.data;
ABC: .stringz "ABC def";
TWO: .stringz "two";

.code;

push ABC;
syscall print_str; syscall print_endl; # ABC def

push TWO;
syscall print_str; syscall print_endl; # two

push 3;
syscall print_i64; syscall print_endl; # 3\n

push 3;
push 55;
swap;
syscall print_i64; syscall print_endl; # 3\n
syscall print_i64; syscall print_endl; # 55\n

push 7;
push 17;
push 53;
get_local 1;
syscall print_i64; syscall print_endl; # 17\n
push 91;
set_local 1;
syscall print_i64; syscall print_endl; # 53\n
syscall print_i64; syscall print_endl; # 91\n
syscall print_i64; syscall print_endl; # 7\n

push 157;
push 255;
lt_i64;
syscall print_i64; syscall print_endl; # 1\n

push 81;
push -4;
lt_i64;
syscall print_i64; syscall print_endl; # 0\n

push 8;
push -15;
add_u64;
syscall print_i64; syscall print_endl; # -7\n

push -100;
push -157;
sub_u64;
syscall print_i64; syscall print_endl; # 57\n

push -14;
push 3;
mul_u64;
syscall print_i64; syscall print_endl; # -42\n

push 17;
push 8;
div_u64;
syscall print_i64; syscall print_endl; # 2\n

push -30;
push 2;
div_i64;
syscall print_i64; syscall print_endl; # -15\n

jmp JMP_PAST;
push 1111;
syscall print_i64; syscall print_endl;
JMP_PAST:

push 0;
jz JZ_PAST;
push 2222;
syscall print_i64; syscall print_endl;
JZ_PAST:

push -1;
jnz JNZ_PAST;
push 3333;
syscall print_i64; syscall print_endl;
JNZ_PAST:

syscall time_current_ms;
push 1000;
lt_i64; # Will exit with 0 code.

exit;
