# UVM Assembly to C Transpiler

`toc` transpiles a subset of [UVM](https://github.com/maximecb/uvm) assembly to C11 source code.

As C (compiled by `gcc` with `-O3`), the UVM `loop.asm` example ran around 4-5x faster than with `uvm` (built with `--release`).

There are not explicit lexing/parsing phases and the assembly is handled in one pass.

## Usage

```sh
$ ./bin/toc path/to/foo.asm > a.c
$ gcc -std=c11 a.c
$ ./a.out
```

## Why

Because it could be done and there were some interesting parts.

## License

Apache-2.0 (same as `uvm`)  
See `LICENSE.txt`
