#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

# Pass a path to a .asm file that is expected to fail transpilation.
ShouldNotTranspile() {
	local asmPath=$1

	# Transpilation will fail for a nonexistent file
	#  so we need an additional check that the file exists.
	if [ ! -f "$asmPath" ] ; then
		echo "FAIL: The test script specifies a file that does not exist: \"$asmPath\""
		echo "      Ensure the file exists or update the test script."
		exit 1
	fi

	# Capture return code in a variable.
	local rc=0
	./bin/toc $asmPath > /dev/null 2>&1 || rc=$?

	if [ $rc -eq 0 ] ; then
		# Transpilation succeeded when it should have failed.
		# Run again but show stdout and stderr.
		./bin/toc $asmPath
		echo "FAIL: Expected \"$asmPath\" to fail to transpile but succeeded."
		exit 1
	elif [ $rc -ne 1 ] ; then
		# Unexpected failure e.g. segfault rc=139 on some systems.
		# All expected failures have an exit code of 1.
		# Run again but show stdout and stderr.
		! ./bin/toc $asmPath
		echo "FAIL: Expected transpilation of \"$asmPath\" to fail and exit \
with code 1 but got code $rc"
		exit 1
	fi
}

# Pass a path to a .asm file that is expected to transpile successfully.
ShouldTranspile() {
	local asmPath=$1

	if ! ./bin/toc $asmPath > /dev/null 2>&1 ; then
		# Transpilation failed when it should have succeeded.
		# Run again but show stdout and stderr.
		! ./bin/toc $asmPath
		echo "FAIL: Expected \"$asmPath\" to transpile successfully but failed."
		exit 1
	fi
}

Compile() {
	local asmPath=$1

	./bin/toc "$asmPath" > temp/a.c
	gcc -std=c11 -g -Wall -Wextra -o temp/a.out temp/a.c
}

CleanCompiled() {
	rm temp/a.c
	rm temp/a.out
}

Run() {
	local asmPath=$1
	local expected=$2

	Compile "$asmPath"

	# Capture return code.
	rc=0
	./temp/a.out > /dev/null || rc=$?

	if [ ! "$rc" -eq 0 ] ; then
		echo "$asmPath had non-zero exit code: $rc"
		exit 1;
	fi

	actual=`./temp/a.out`
	if [ "$actual" != "$expected" ] ; then
		echo "$asmPath failed."
		echo "Expected:"
		echo -n "$expected"
		echo "Actual:"
		echo -n "$actual"
		exit 1
	fi

	CleanCompiled
}

##### ##### ##### ##### #####

ShouldNotTranspile "./testasm/F_DataAfterCode.asm"
ShouldNotTranspile "./testasm/F_InvalidCommandChar.asm"
ShouldNotTranspile "./testasm/F_UnknownCommand.asm"

ShouldTranspile "./testasm/P_alpha.asm"

rm -f "./temp/a.c"
rm -f "./temp/a.out"

# Remember: Trailing newlines get stripped from output capture. Ugh.

expected="ABC def
two
3
3
55
17
53
91
7
1
0
-7
57
-42
2
-15"
Run "./testasm/P_arith.asm" "$expected"

Run "./testasm/P_call.asm" ""

echo "All tests PASSED"
