bin/toc: toc.c | bin
	gcc -o $@ -std=c11 -g -Wall -Wextra toc.c \
		-Wno-pointer-sign \
		-Wno-type-limits

bin:
	mkdir bin

temp:
	mkdir temp

.PHONY: clean
clean:
	rm -f bin/toc

.PHONY: test
test: bin/toc | temp
	./test.sh
